//
//  main.c
//  Exam
//
//  Created by y-fujiwara on 2018/09/28.
//  Copyright © 2018年 y-fujiwara. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>
#include "pthread.h"
#include <signal.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

// 各スレッドで書き込みを行う配列のサイズを計算するマクロ
// 計算分が1秒間に生成される数字の数だけどバッファとして100文字分取っておく
#define num_size(m, sleep) (((1000000 / sleep) * m) + 100)

const int N = 1;
const int M = 2;
const int NUM_LIMIT = 10;
const int READ = 0;
const int WRITE = 1;
const int PING = 1;
const int THREAD_SLEEP = 10000;
const long INVALID_TIME = 10000000000000;
volatile sig_atomic_t e_flag = 0;

// テスト用関数の前方宣言
// TODO: そもそもテスト関数を外出するべきだが面倒なのでとりあえずmain関数の下に移したので
void test();

// mutexオブジェクトはグローバルにしたとしてもプロセスごとに独立になるはず
pthread_mutex_t mutex;

typedef struct {
    time_t time;
    int count[NUM_LIMIT];
} Result;

void abrt_handler(int sig) {
    e_flag = 1;
}

// 無限ループの中にあるとテストしにくいので切り出し
void copy_rand(char* ret) {
    sprintf(ret, "%s%d", ret, rand() % NUM_LIMIT);
}

// pipeを使うことを考えて文字列を想定しておく
void* gen_rand(void* ret) {
    while(1) {
        pthread_mutex_lock(&mutex);
        copy_rand((char *)ret);
        pthread_mutex_unlock(&mutex);
        usleep(THREAD_SLEEP);
    }
}

void to_json(Result* ret, char* ret_json) {
    // 構造体がNULLだったら適当に数値を入れてしまう
    if (ret == NULL) {
        sprintf(ret_json, "{'time': %ld,\n 'counts': {'0': %d, '1': %d, '2': %d, '3': %d, '4': %d, '5': %d, '6': %d, '7': %d, '8': %d, '9': %d}}",
                INVALID_TIME,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0);

    } else {
        sprintf(ret_json, "{'time': %ld,\n 'counts': {'0': %d, '1': %d, '2': %d, '3': %d, '4': %d, '5': %d, '6': %d, '7': %d, '8': %d, '9': %d}}",
                ret->time,
                ret->count[0],
                ret->count[1],
                ret->count[2],
                ret->count[3],
                ret->count[4],
                ret->count[5],
                ret->count[6],
                ret->count[7],
                ret->count[8],
                ret->count[9]);
    }
}

void start_child_process(int m, int* pipe_aggregate, int* my_pipe) {
    pid_t pid = getpid();
    char nums[num_size(m, THREAD_SLEEP)];
    nums[0] = '\0';
    pthread_t threads[m];
    
    srand(pid);
    pthread_mutex_init(&mutex, NULL);

    // m個のスレッドを立ち上げる
    for (int i = 0; i < m; i++) {
        pthread_create(&threads[i], NULL, &gen_rand, nums);
    }

    while (1) {
        int buf;
        read(my_pipe[READ], &buf, sizeof(buf));
        switch (buf) {
            case PING:
                write(pipe_aggregate[WRITE], nums, sizeof(nums));
                pthread_mutex_lock(&mutex);
                nums[0] = '\0';
                pthread_mutex_unlock(&mutex);
                break;
            default:
                write(pipe_aggregate[WRITE], "nothing", sizeof("nothing"));
                break;
        }
    }
}

// mが間違っていて、文字配列からあふれる場合は対応できない。実際に立てられたスレッド数はスレッド管理プロセスでしかわからないため
// また、多重配列を引数に渡す場合にポインタが渡る関係でsizeof演算が簡単にできない点に注意(l.132のコメントもあるが不整合のチェックが難しい)
void start_aggregate_process(int idx, int m, int pipe_children[idx][2], int* pipe_aggregate) {
    // TODO: 引数のidxとpipeの数はあっている保証が無いので中で計算したい
    int n = idx;
    while(1) {
        Result ret;

        // 初期化 インクリメントするので初期化しないとおかしくなる
        for (int i = 0; i < NUM_LIMIT; i++) {
            ret.count[i] = 0;
        }

        // timeを設定してから1秒待つこと
        ret.time = time(NULL);
        sleep(1);

        for (int i = 0; i < n; i++) {
            char res[num_size(m, THREAD_SLEEP)];
            write(pipe_children[i][WRITE], &PING, sizeof(PING));
            read(pipe_aggregate[READ], res, sizeof(res));
            int index = 0;
            
            while (res[index] != '\0') {
                int n = res[index] - '0';
                ret.count[n]++;
                index++;
            }
        }

        char json[256];
        to_json(&ret, json);
        // TODO: 標準出力だと最終的にテストがしにくいので出力だけするプロセスを立ち上げてそこにpipeでつなげるのが良いかも
        printf("%s\n", json);
    }
}

int main(int argc, const char * argv[]) {
    // デフォルトはプロセスもスレッド数も2とする
    int n = 2, m = 2;
    pid_t p_pid = getpid();
    pid_t aggr_pid;

    // 引数でなにか指定されていたらそちらを使う
    if (argc == 3) {
        n = atoi(argv[N]);
        m = atoi(argv[M]);
    } else if (argc == 2 && strcmp(argv[1], "test") == 0) {
        test();
        exit(0);
    }

    // 子プロセス用パイプ配列
    int pipe_children[n][2];
    // 集計用パイプ
    int pipe_aggregate[2];
    
    // プロセスが分岐するより先にパイプを作ること
    if (pipe(pipe_aggregate) < 0) {
        perror("pipe aggregate");
        close(pipe_aggregate[READ]);
        close(pipe_aggregate[WRITE]);
        exit(-1);
    }
    
    for (int i = 0; i < n; i++) {
        if (pipe(pipe_children[i]) < 0) {
            perror("pipe children");
            close(pipe_children[i][READ]);
            close(pipe_children[i][WRITE]);
            exit(-1);
        }
    }
   
    // nプロセス分のPID配列
    pid_t pids[n];
    pid_t wait_p;
   
    for (int i = 0; i < n; i++) {
        // fork処理は親プロセスが行う
        if (getpid() == p_pid) {
            switch(pids[i] = fork()) {
                case 0:
                    start_child_process(m, pipe_aggregate, pipe_children[i]);
                    break;
                case -1:
                    perror("fork\n");
                    break;
                default:
                    printf("[%d]child = %d\n", p_pid, pids[i]);
                    break;
            }
        }
    }
    
    // fork処理は親プロセスが行う
    if (getpid() == p_pid) {
        // 集計用プロセス起動
        switch(aggr_pid = fork()) {
            case 0:
                start_aggregate_process(n, m, pipe_children, pipe_aggregate);
                break;
            case -1:
                perror("fork\n");
                break;
            default:
                printf("[%d]child_aggr = %d\n", p_pid, aggr_pid);
                break;
        }
    }
   
    if (signal(SIGINT, abrt_handler) == SIG_ERR) {
        exit(1);
    }
    while (!e_flag) {}
   
    // CTRL + Cで止められる場合は子プロセスをkillする
    for (int i = 0; i < n; i++) {
        kill(pids[i], SIGKILL);
    }
    kill(aggr_pid, SIGKILL);
    printf("\n");

    // killされた子プロセスをn + 1回分だけwait
    for (int i = 0; i <= n; i++) {
        wait_p = wait(0);
        printf("end = %d\n", wait_p);
    }
 
    close(pipe_aggregate[READ]);
    close(pipe_aggregate[WRITE]);
    
    for (int i = 0; i < n; i++) {
        close(pipe_children[i][READ]);
        close(pipe_children[i][WRITE]);
    }

    return 0;
}

void child_process_test() {
    int m = 2;
    int my_pipe[2];
    int pipe_agr[2];
    pid_t child_pid;
    pid_t wait_p;

    printf("child test start\n");
    printf("====================================\n");

    if (pipe(my_pipe) < 0) {
        close(my_pipe[WRITE]);
        close(my_pipe[READ]);
    }

    if (pipe(pipe_agr) < 0) {
        close(pipe_agr[WRITE]);
        close(pipe_agr[READ]);
    }

    switch(child_pid = fork()) {
        case 0:
            // start_aggregate_process(n, m, pipe_dummy, pipe_agr);
            start_child_process(m, pipe_agr, my_pipe);
            break;
        case -1:
            perror("fork\n");
            break;
        default:
            break;
    }
    char res[num_size(m, THREAD_SLEEP)];
    res[0] = '\0';

    int temp = 100;
    // PINGメッセージ以外を送っても何も起きない
    printf("child process send to nothing: ");
    write(my_pipe[WRITE], &temp, sizeof(temp));
    read(pipe_agr[READ], res, sizeof(res));
    assert(strstr(res, "nothing") != NULL);
    printf("ok\n");
    res[0] = '\0';

    printf("child process send to ping: ");
    sleep(1);
    write(my_pipe[WRITE], &PING, sizeof(PING));
    read(pipe_agr[READ], res, sizeof(res));
    int idx = 0;
    // read結果が全部数字なこと
    while (res[idx] != '\0') {
        assert(isdigit(res[idx]));
        idx++;
    }
    // 一つ以上生成されていること
    assert(idx > 0);
    printf("ok\n\n");
    res[0] = '\0';

    // 異常系
    printf("child invalid test start\n");
    printf("====================================\n");
    printf("child process send after not read: ");
    // こいつのレスポンスはnothing
    write(my_pipe[WRITE], &temp, sizeof(temp));
    sleep(1);
    // readするまえにwriteする
    write(my_pipe[WRITE], &PING, sizeof(PING));
   
    // 一回目のread時は一回目のnothingが入っている
    read(pipe_agr[READ], res, sizeof(res));
    assert(strstr(res, "nothing") != NULL);
    res[0] = '\0';

    // 二回目の読み込み時には数字文字列が帰ってくる
    idx = 0;
    read(pipe_agr[READ], res, sizeof(res));
    // read結果が全部数字なこと
    while (res[idx] != '\0') {
        assert(isdigit(res[idx]));
        idx++;
    }
    // 一つ以上生成されていること
    assert(idx > 0);
    printf("ok\n");

    kill(child_pid, SIGKILL);
    printf("\n");
    wait_p = wait(0);
    printf("end = %d\n", wait_p);
}

// 集計プロセスのテスト
void aggregate_test() {
    printf("aggreagate test start\n");
    printf("====================================\n");
    printf("aggregate process send to ping: ");
    int n = 1, m = 1;

    // pipeをスタブとしている感じ
    int pipe_dummy[0][2];
    int pipe_agr[2];
    pid_t aggr_pid;
    pid_t wait_p;

    char nums[num_size(m, THREAD_SLEEP)];
    nums[0] = '\0';

    if (pipe(pipe_dummy[0]) < 0) {
        close(pipe_dummy[0][WRITE]);
        close(pipe_dummy[0][READ]);
    }

    if (pipe(pipe_agr) < 0) {
        close(pipe_agr[WRITE]);
        close(pipe_agr[READ]);
    }
    
    switch(aggr_pid = fork()) {
        case 0:
            start_aggregate_process(n, m, pipe_dummy, pipe_agr);
            break;
        case -1:
            perror("fork\n");
            break;
        default:
            break;
    }

    // 1秒待てばPINGがされていること
    for (int i = 0; i < 2; i++) {
        sleep(1);
        int buf;
        read(pipe_dummy[0][READ], &buf, sizeof(buf));
        assert(buf == PING);
        printf("ok,");
        // 特に意味がないけど書き込みだけする
        write(pipe_agr[WRITE], nums, sizeof(nums));
    }

    kill(aggr_pid, SIGKILL);
    printf("\n");
    wait_p = wait(0);
    printf("end = %d\n", wait_p);

    printf("\n");

    // 異常系
    //  printf("aggreagate invalid test start\n");
    //  printf("====================================\n");
    //  printf("invalid args: ");
    // 
    // nとmに変な値が入れられているときは配列の大きさの方を優先する
    // switch(aggr_pid = fork()) {
    //     case 0:
    //         start_aggregate_process(2, 2, pipe_dummy, pipe_agr);
    //         break;
    //     case -1:
    //         perror("fork\n");
    //         break;
    //     default:
    //         break;
    // }

    // // n,mが実際に立ち上げれれるスレッドなどの数と異なっていてもちゃんと動くこと
    // for (int i = 0; i < 2; i++) {
    //     sleep(1);
    //     int buf;
    //     read(pipe_dummy[0][READ], &buf, sizeof(buf));
    //     assert(buf == PING);
    //     printf("ok,");
    //     // 特に意味がないけど書き込みだけする
    //     write(pipe_agr[WRITE], nums, sizeof(nums));
    // }

    // kill(aggr_pid, SIGKILL);
    // printf("\n");
    // wait_p = wait(0);
    // printf("end = %d\n", wait_p);
    // printf("\n");
}

void test() {
    int init_nums = 10;
    printf("unit test start\n");
    printf("====================================\n");

    // 基本正常系
    printf("generate rand init: ");
    char nums[init_nums];
    nums[0] = '\0';
    assert(nums[0] == '\0');
    printf("ok\n");

    // 一回ランダム値を生成したときに配列が一個増えていること
    printf("generate rand first: ");
    copy_rand(nums);
    // 先頭1文字が数字になっていればOK
    assert(isdigit(nums[0]));
    printf("ok\n");

    // JSONのキーとしてcountsとtimeがあるか
    // 一旦面倒なので入れ子構造とかみたいな並び順は見ない
    char json[256];
    Result ret;
    printf("generate json include 'time', 'counts' and number keys: ");
    to_json(&ret, json);
    assert(strstr(json, "'time'") != NULL);
    assert(strstr(json, "'counts'") != NULL);
    assert(strstr(json, "'0'") != NULL);
    assert(strstr(json, "'1'") != NULL);
    assert(strstr(json, "'2'") != NULL);
    assert(strstr(json, "'3'") != NULL);
    assert(strstr(json, "'4'") != NULL);
    assert(strstr(json, "'5'") != NULL);
    assert(strstr(json, "'6'") != NULL);
    assert(strstr(json, "'7'") != NULL);
    assert(strstr(json, "'8'") != NULL);
    assert(strstr(json, "'9'") != NULL);
    printf("ok\n");

    // 構造体にtimeがセットしてあるときにちゃんと出ているか
    printf("set time to json: ");
    ret.time = 1538283201;
    to_json(&ret, json);
    assert(strstr(json, "1538283201") != NULL);
    printf("ok\n");

    // 構造体にcountsがセットしてあるときにちゃんと出ているか
    printf("set counts to json: ");
    for (int i = 0; i < 10; i++) {
        ret.count[i] = 1;
    }
    to_json(&ret, json);
    assert(strstr(json, "'0': 1") != NULL);
    assert(strstr(json, "'1': 1") != NULL);
    assert(strstr(json, "'2': 1") != NULL);
    assert(strstr(json, "'3': 1") != NULL);
    assert(strstr(json, "'4': 1") != NULL);
    assert(strstr(json, "'5': 1") != NULL);
    assert(strstr(json, "'6': 1") != NULL);
    assert(strstr(json, "'7': 1") != NULL);
    assert(strstr(json, "'8': 1") != NULL);
    assert(strstr(json, "'9': 1") != NULL);
    printf("ok\n\n");

    printf("invalid unit test start\n");
    printf("====================================\n");
    printf("struct is NULL: ");
    to_json(NULL, json);
    assert(strstr(json, "10000000000000") != NULL);
    assert(strstr(json, "'0': 0") != NULL);
    assert(strstr(json, "'1': 0") != NULL);
    assert(strstr(json, "'2': 0") != NULL);
    assert(strstr(json, "'3': 0") != NULL);
    assert(strstr(json, "'4': 0") != NULL);
    assert(strstr(json, "'5': 0") != NULL);
    assert(strstr(json, "'6': 0") != NULL);
    assert(strstr(json, "'7': 0") != NULL);
    assert(strstr(json, "'8': 0") != NULL);
    assert(strstr(json, "'9': 0") != NULL);
    printf("ok\n\n");

    aggregate_test();
    printf("\n");
    child_process_test();
}

