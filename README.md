# Exam

## requirements

* gcc

## usage

### BUILD & RUN

* BUILD
  * `$ cd src`
  * `$ gcc -o main main.c`
* RUN
  * `$ ./main PROCESS_NUM THREADS_NUM`
    * ex. `$ ./main 4 4`
      * 4 threads in 4 process
* STOP
  * ctrl + c
    * NOTICE: if `$ kill -9 pid` leaving a zombie process
* TEST
  * `$ ./main test`
